<a name="1.7.1"></a>
## [1.7.1](https://gitlab.com/5stones/scylla-bigcommerce/compare/v1.7.0...v1.7.1) (2021-01-28)


### Bug Fixes

* **tasks:** fix checking for `after` option ([cccb525](https://gitlab.com/5stones/scylla-bigcommerce/commit/cccb525))



<a name="1.7.0"></a>
# [1.7.0](https://gitlab.com/5stones/scylla-bigcommerce/compare/v1.6.0...v1.7.0) (2020-12-03)


### Features

* **bundleb2b:** map extraFields to top level properties ([8a357ff](https://gitlab.com/5stones/scylla-bigcommerce/commit/8a357ff))



<a name="1.6.0"></a>
# [1.6.0](https://gitlab.com/5stones/scylla-bigcommerce/compare/v1.5.0...v1.6.0) (2020-11-11)


### Features

* **bundleb2b:** pull in company data if bundleb2b_token is configured ([c05dd7f](https://gitlab.com/5stones/scylla-bigcommerce/commit/c05dd7f))



<a name="1.5.0"></a>
# [1.5.0](https://gitlab.com/5stones/scylla-bigcommerce/compare/v1.4.1...v1.5.0) (2019-11-21)


### Bug Fixes

* **tasks:** fix _last_order_date on new customers ([2feb201](https://gitlab.com/5stones/scylla-bigcommerce/commit/2feb201))
* refactor to fix error handling ([3f9a60f](https://gitlab.com/5stones/scylla-bigcommerce/commit/3f9a60f))


### Features

* **tasks:** add _last_order_date to Customer ([2df8f89](https://gitlab.com/5stones/scylla-bigcommerce/commit/2df8f89))



<a name="1.4.1"></a>
## [1.4.1](https://gitlab.com/5stones/scylla-bigcommerce/compare/v1.4.0...v1.4.1) (2019-04-24)


### Bug Fixes

* **BigCommerceRestClient:** Fix issue with pagination getting broken due to class variable ([ba37846](https://gitlab.com/5stones/scylla-bigcommerce/commit/ba37846))



<a name="1.4.0"></a>
# [1.4.0](https://gitlab.com/5stones/scylla-bigcommerce/compare/v1.3.0...v1.4.0) (2019-04-24)


### Features

* **BigCommerceLastModifiedTask, app:** Add a task to download records based on the date the last re ([88c60a3](https://gitlab.com/5stones/scylla-bigcommerce/commit/88c60a3))



<a name="1.3.0"></a>
# [1.3.0](https://gitlab.com/5stones/scylla-bigcommerce/compare/v1.2.0...v1.3.0) (2019-04-12)


### Features

* **App:** Remove filter on status code for download task ([05ccbc9](https://gitlab.com/5stones/scylla-bigcommerce/commit/05ccbc9))



<a name="1.2.0"></a>
# [1.2.0](https://gitlab.com/5stones/scylla-bigcommerce/compare/v1.1.0...v1.2.0) (2019-02-13)


### Features

* **BigCommerceRestClient, BigCommerceRecord, BigCommerceTask:** Paginate through sub resources ([71c9a10](https://gitlab.com/5stones/scylla-bigcommerce/commit/71c9a10))



<a name="1.1.0"></a>
# [1.1.0](https://gitlab.com/5stones/scylla-bigcommerce/compare/v1.0.2...v1.1.0) (2019-02-13)


### Features

* **App, BigCommerceTask:** Add the ability to pass additional query parameters per task ([87f4da0](https://gitlab.com/5stones/scylla-bigcommerce/commit/87f4da0))



<a name="1.0.2"></a>
## [1.0.2](https://gitlab.com/5stones/scylla-bigcommerce/compare/v1.0.1...v1.0.2) (2019-01-21)


### Bug Fixes

* **ToBigCommerceTask:** Fix issue with RecordError generation on ToBigCommerceTasks ([6959396](https://gitlab.com/5stones/scylla-bigcommerce/commit/6959396))



<a name="1.0.1"></a>
## [1.0.1](https://gitlab.com/5stones/scylla-bigcommerce/compare/v1.0.0...v1.0.1) (2019-01-09)


### Bug Fixes

* **ToBigCommerceTask:** Fix extra method parameter ([c15eed0](https://gitlab.com/5stones/scylla-bigcommerce/commit/c15eed0))



<a name="1.0.0"></a>
# [1.0.0](https://gitlab.com/5stones/scylla-bigcommerce/compare/e186092...v1.0.0) (2019-01-08)


### Features

* **App, BigCommerceTask, BigCommerceRestClient, BigCommerceRecord:** Add the ability to download or ([e186092](https://gitlab.com/5stones/scylla-bigcommerce/commit/e186092))
* **App, ToBigCommerceTask, ToBigCommerceShipmentTask:** Add an on-demand task to pull down states/c ([daa1133](https://gitlab.com/5stones/scylla-bigcommerce/commit/daa1133))
* **BigCommerceCustomer, App, BigCommerceTask:** Add the ability to download customers from BigComme ([32b428c](https://gitlab.com/5stones/scylla-bigcommerce/commit/32b428c))
* **BigCommerceTask,  BigCommerceProduct, App:** Add the ability to download products ([7e106bc](https://gitlab.com/5stones/scylla-bigcommerce/commit/7e106bc))
* **BigCommerceTask, ToBigCommerceTask:** Add configurable pagination, add an _after_save hook after ([13879f6](https://gitlab.com/5stones/scylla-bigcommerce/commit/13879f6))
* **ToBigCommerceProductTask:** Add a task to update/create products ([e54f21f](https://gitlab.com/5stones/scylla-bigcommerce/commit/e54f21f))



