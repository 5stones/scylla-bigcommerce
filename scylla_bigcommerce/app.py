import scylla
from .client import BigCommerceRestClient
from .tasks import BigCommerceTask, BigCommerceLastModifiedTask, BundleB2bCompanyTask
from .b2b_client import BundleB2bRestClient


scylla.configuration.setDefaults('BigCommerce', {
    #'client_id': None,
    #'client_secret': None,
    #'token': None,
    'debug': False,
    'verify_ssl': True,
    'version': 'v2',
    'tasks': ['Customer', 'Order', 'Product'],
})


class App(scylla.App):

    task_params = [
        { 'klass': 'BigCommerceTask', 'params': ('Country',) },
        { 'klass': 'BigCommerceTask', 'params': ('Customer',) },
        { 'klass': 'BigCommerceTask', 'params': ('Product',) },
        { 'klass': 'BigCommerceLastModifiedTask', 'params': ('Order',) },
    ]

    def _prepare(self):
        config = scylla.configuration.getSection('BigCommerce')
        self.client = BigCommerceRestClient(
            config.get('name'),
            config.get('url'),
            config.get('client_secret'),
            config.get('client_id'),
            version=config.get('version'),
            verify_ssl=config.get('verify_ssl'),
            debug=config.get('debug')
        )

        b2b_token = config.get('bundleb2b_token', None)
        b2b_client = BundleB2bRestClient(b2b_token, debug=config.get('debug')) if b2b_token else None

        for task_params in self.task_params:
            task = self._task_factory(task_params['klass'], task_params['params'])
            task.b2b_client = b2b_client
            task_name = task_params['params'][0]
            if task_name in config.get('tasks'):
                self.tasks[task_name] = task
            else:
                self.on_demand_tasks[task_name] = task

        if b2b_client:
            self._prepare_bundleb2b(b2b_client)

    def _prepare_bundleb2b(self, b2b_client):
        # their minLastModifiedTime is causing 500 errors, so we can't run this regularly right now
        self.on_demand_tasks['Company'] = BundleB2bCompanyTask(self.client, b2b_client, page_size=100)

    def _task_factory(self, klass_name, task_params):
        klass = globals()[klass_name]
        return  klass(self.client, *task_params)
