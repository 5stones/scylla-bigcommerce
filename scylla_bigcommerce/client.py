import logging
import requests
import scylla


class BigCommerceRestError(scylla.RestClient.RestError):
    pass


class BigCommerceRestClient(scylla.RestClient):
    _logger = logging.getLogger('scylla-bigcommerce')

    _successful_codes = [200, 201, 204]
    RestError = BigCommerceRestError

    _subresource_url_mapping = {
        'shippingaddresses': 'shipping_addresses',
        'shippingquotes': 'shipping_quotes',
    }

    def __init__(self, name, url, client_secret, client_id, version='v2', verify_ssl=False, debug=False):
        self.version = version
        url = url + '/{0}'.format(self.version)
        super(BigCommerceRestClient, self).__init__(
            url,
            verify_ssl=verify_ssl,
            debug=debug
        )
        self._set_header('X-Auth-Token', client_secret)
        self._set_header('X-Auth-Client', client_id)
        self.name = name
        if debug:
            self._logger.setLevel(logging.DEBUG)

    def post(self, path, params=None, data=None):
        endpoint = u'{}{}'.format(self.base_url, path)

        self._logger.debug('POST %s %s', endpoint, (params, data))

        response = requests.post(
            endpoint,
            params=params,
            json=data,
            **self._base_request
        )

        return self._process_response(response)

    def put(self, path, params=None, data=None):
        endpoint = u'{}{}'.format(self.base_url, path)

        self._logger.debug('PUT %s %s', endpoint, (params, data))

        response = requests.put(
            endpoint,
            params=params,
            json=data,
            **self._base_request
        )

        return self._process_response(response)

    def get_resource(self, resource, process_page=None, carry=None):
        path = resource.get('resource')

        for key, value in self._subresource_url_mapping.iteritems():
            path = path.replace(key, value)

        if process_page:
            self.get_paginated(path, process_page, carry=carry)
            return None

        # load all pages of results
        results = []
        for data in self.get_iterator(path):
            if isinstance(data, list):
                results.extend(data)
            elif data:
                return data;
        return results

    def get_paginated(self, path, process_page, args=None, carry=None):
        for results in self.get_iterator(path, args):
            process_page(results, carry=carry)

    def get_iterator(self, path, params=None):
        """Iterate over pages of results for a GET path."""
        if path[0] != '/':
            path = u'/{}'.format(path)

        # paginate using a normal get request
        request = lambda p: self.get(path, p)
        if self.version == 'v2':
            return _PaginatedRequestV2(request, params)
        #elif self.version == 'v3':
        #    return _PaginatedRequestV3(request, params)
        else:
            raise Exception('Unsupported API version: {}'.format(self.version))


class _PaginatedRequestV2(object):
    def __init__(self, request, params=None):
        self.request = request
        self.params = params or {'page': 1, 'limit': 30}
        self.is_finished = False

    def __iter__(self):
        return self

    def next(self):
        """Get the next page of results."""
        if self.is_finished:
            raise StopIteration
        try:
            results = self.request(self.params)
        except ValueError:
            # successful request with an empty response
            raise StopIteration

        self.params['page'] = self.params.get('page', 1) + 1
        if 'limit' in self.params and len(results) < self.params['limit']:
            # less than a full page of results, so assume it's the last page
            self.is_finished = True

        return results


class _PaginatedRequestV3(object):
    def __init__(self, request, params=None):
        self.request = request
        self.params = params or {}
        self.is_finished = False

    def __iter__(self):
        return self

    def next(self):
        """Get the next page of results."""
        if self.is_finished:
            raise StopIteration

        results = self.request(self.params)

        try:
            pagination = results['meta']['pagination']
            if pagination['current_page'] == pagination['total_pages']:
                self.is_finished = True
            else:
                self.params['page'] = pagination['current_page'] + 1
        except KeyError:
            self.is_finished = True

        return results.get('data')
