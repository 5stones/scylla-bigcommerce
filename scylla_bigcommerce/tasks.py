import logging
import scylla
from scylla import orientdb
from datetime import datetime
from datetime import timedelta
from .records import BigCommerceRecord


class BigCommerceTask(scylla.Task):
    """Downloads records from BigCommerce.
    """
    api_types = {
        'Customer': 'customers',
        'Order': 'orders',
        'Product': 'products',
        'Country': 'countries',
    }

    _logger = logging.getLogger('scylla-bigcommerce')

    def __init__(self, client, record_type, record_prefix='BigCommerce', page_size=30, additional_filters={}):
        self.record_type = record_type
        self.client = client
        self.classname = '{0}{1}'.format(record_prefix, self.record_type)
        self.api_type = self.api_types[self.record_type]
        self.page_size = page_size
        self.additional_filters = additional_filters
        self.b2b_client = None

        task_id = '{0}-download'.format(self.classname)
        super(BigCommerceTask, self).__init__(task_id)

    def _step(self, after, options):
        args = {
            'page': 1,
            'limit': self.page_size,
        }

        args.update(self.additional_filters)

        if after:
            args['min_date_modified'] = after

        self._logger.info(u'Getting %s updated after %s', self.api_type, after)
        self.client.get_paginated(self.api_type, self._process_search_response, args=args)

    def process_one(self, rec_id):
        response = self.client.get('/{}/{}'.format(self.api_type, rec_id))
        self._process_response_record(response)

    def _process_search_response(self, response, carry=None):
        for obj in response:
            self._process_response_record(obj)

    def _process_response_record(self, obj):
        rec = BigCommerceRecord.factory(self.client, self.record_type, obj)
        with scylla.ErrorLogging(self.task_id, rec):
            self._fill_additional(rec)
            rec.throw_at_orient()
            self._link_children(rec)
            self._update_customer(rec)
            self._logger.info(u'Saved %s %s as %s %s',
                              self.record_type, obj.get(rec.key_field),
                              rec.classname, rec.rid)

    def _link_children(self, rec):
        """Once the record and subrecords are in orientdb, add _parent to children
            and children of children recursively, depth first.
        """
        # pull rids of children
        rids = []
        for subtype in rec._sub_records:
            value = rec.get(subtype)
            if isinstance(value, BigCommerceRecord):
                # single record
                rids.append(value.rid)
                # recursively link children of child
                self._link_children(value)
            elif value:
                # list of records
                for subrecord in value:
                    rids.append(subrecord.rid)
                    # recursively link children of child
                    self._link_children(subrecord)
        # run the update in orientdb
        orientdb.execute("UPDATE [{}] SET _parent = {}".format(','.join(rids), rec.rid))

    def _fill_additional(self, rec):
        if self.record_type == 'Customer' and not rec.rid:
            # determine _last_order_date for customers created after the order
            orders_after = rec['date_modified'] - timedelta(hours=1)
            result = orientdb.execute((
                "SELECT date_created "
                "FROM {}Order "
                "WHERE date_created >= '{}' AND customer_id = {} "
                "ORDER BY date_created DESC "
                "LIMIT 1"
            ).format(self.client.name, orders_after.date(), rec['id']))
            if result:
                rec.data['_last_order_date'] = result[0].get('date_created')

        if self.b2b_client:
            if self.record_type == 'Order':
                self._fill_company(rec)

    def _fill_company(self, rec):
        (rec.data['_company_metadata'], company) = self.b2b_client.get_company_for_order(rec['id'])
        if company:
            rec.data['_company'] = BigCommerceRecord.factory(self.client, 'Company', company)

    def _update_customer(self, rec):
        if self.record_type == 'Order':
            # update the customer's order date so we can filter syncs by it
            try:
                customer_id = rec['customer_id']
                order_date = str(rec['date_created']).replace(' ', 'T')[:19] + 'Z'

                orientdb.execute((
                    "UPDATE {}Customer "
                    "SET _last_order_date = '{}', _updated_at = date() "
                    "WHERE id = '{}'"
                ).format(self.client.name, order_date, customer_id))
            except KeyError:
                pass


class BigCommerceLastModifiedTask(BigCommerceTask):
    """Downloads records from BigCommerce based on the most recent date_modified of that record type.
    """

    max_minutes_offset = 2

    def _step(self, after, options):
        args = {
            'page': 1,
            'limit': self.page_size,
        }

        args.update(self.additional_filters)

        after_string = options.get('after')
        if after_string is None:
            after_string = self._get_last_modified_date()
        if after_string:
            args['min_date_modified'] = after_string

        # only pull records up to x minutes ago to avoid records that aren't completely saved
        max_time = (datetime.utcnow().replace(microsecond=0) - timedelta(minutes=self.max_minutes_offset))
        args['max_date_modified'] = '{}Z'.format(max_time.isoformat())

        self._logger.info('Getting %s updated after date_modified %s and before date_modified %s:',
                          self.api_type, after_string, args['max_date_modified'])
        self.client.get_paginated(self.api_type, self._process_search_response, args=args)

    def _get_last_modified_date(self, field='date_modified'):
        q = (
            'SELECT {field} '
            'FROM {record_type} '
            'ORDER BY {field} DESC '
            'LIMIT 1 '
        ).format(field=field, record_type='{}{}'.format(self.client.name, self.record_type))

        result = orientdb.execute(q)
        if len(result) > 0:
            return result[0].get(field)
        else:
            return None


class BundleB2bCompanyTask(scylla.Task):
    """Downloads Company records from BundleB2B.
    """
    _logger = logging.getLogger('scylla-bigcommerce')

    def __init__(self, client, b2b_client, record_prefix='BigCommerce', page_size=10):
        self.record_type = "Company"
        self.client = client
        self.b2b_client = b2b_client
        self.classname = '{0}{1}'.format(record_prefix, self.record_type)
        self.page_size = page_size

        task_id = '{0}-download'.format(self.classname)
        super(BundleB2bCompanyTask, self).__init__(task_id)

    def _step(self, after, options):
        after_string = options['after']
        if after_string:
            after_datetime = scylla.parse_iso_date(after_string)
            after_timestamp = int((after_datetime - datetime(1970, 1, 1)).total_seconds())
        elif after_string is not None:
            # passed empty --after=
            after_timestamp = None
        else:
            (after_string, after_timestamp) = self._get_last_modified_date()

        self._logger.info(u'Getting companies updated after %s', after_string)
        offset = 0
        while offset == 0 or offset < response['pagination']['totalCount']:
            response = self.b2b_client.get_companies(after_timestamp, limit=self.page_size, offset=offset)
            for obj in response.get('list', []):
                # merge in complete info for company
                obj.update(self.b2b_client.get_company(obj['companyId']))
                self._process_response_record(obj)
            offset = offset + self.page_size

    def process_one(self, rec_id):
        response = self.b2b_client.get_company(rec_id)
        self._process_response_record(response)

    def _process_response_record(self, obj):
        rec = BigCommerceRecord.factory(self.client, self.record_type, obj)
        with scylla.ErrorLogging(self.task_id, rec):
            rec.throw_at_orient()
            self._logger.info(u'Saved %s %s as %s %s',
                              self.record_type, obj.get(rec.key_field),
                              rec.classname, rec.rid)

    def _get_last_modified_date(self, field='updatedAt'):
        q = (
            'SELECT {field} AS value, {field}.asDatetime().asLong() AS value_seconds '
            'FROM {record_type} '
            'ORDER BY {field} DESC '
            'LIMIT 1 '
        ).format(field=field, record_type='{}{}'.format(self.client.name, self.record_type))

        result = orientdb.execute(q)
        if len(result) > 0:
            return (result[0].get('value'), int(result[0].get('value_seconds'))/1000)
        else:
            return ('', None)


class ToBigCommerceTask(scylla.ReflectTask):
    """Checks for updates to records an triggers endpoints in bigcommerce.
    """

    def __init__(self,
            from_class,
            client, to_type,
            url_format,
            conversion=None,
            where=None,
            with_reflection=True):

        super(ToBigCommerceTask, self).__init__(
            from_class,
            (client.name, to_type),
            where=where,
            with_reflection=with_reflection)

        self.client = client
        self.url_format = url_format
        self.conversion = conversion

    def _process_response(self, response):
        results = [self._create_record(obj) for obj in response]
        return super(ToBigCommerceTask, self)._process_response(results)

    def _create_record(self, obj):
        rec = scylla.Record(obj)
        rec.key_field = self.key_field
        return rec

    def _process_response_record(self, obj):
        """Hits a BigCommerce endpoint for the result
        """
        # create the record in bigcommerce
        if self.conversion:
            data = self.conversion(obj)
        else:
            data = None

        is_update = obj.get('_linked_to', '') != ''
        bigcommerce_result = self._to_bigcommerce(obj, is_update=is_update, data=data)

        # save the bigcommerce response to orient and link it
        bigcommerce_rec = BigCommerceRecord.factory(self.client, self._get_factory_type(), bigcommerce_result)
        self._save_response(obj, bigcommerce_rec, is_update=is_update, request=data)

    def _save_response(self, from_obj, result_rec, is_update=False, request=None):
        super(ToBigCommerceTask, self)._save_response(from_obj, result_rec, is_update=is_update, request=request)

        # save the response to orientdb
        with scylla.Uninterruptable():
            self._after_save(from_obj, result_rec, is_update=is_update, request=request)

    def _after_save(self, from_obj, result_rec, is_update=False, request=None):
        pass

    def _get_factory_type(self):
        return self.to_type

    def _to_bigcommerce(self, obj, is_update=False, data=None):

        if is_update != False:
            url = self._get_url(obj, is_update=is_update, data=data)
            return self.client.put(url, data=data)
        else:
            url = self._get_url(obj, data)
            return self.client.post(url, data=data)

    def _get_url(self, obj, is_update=False, data=None):
        params = self._get_url_params(obj, data, is_update=is_update)
        return self.url_format.format(*params)

    def _get_url_params(self, obj, data=None, is_update=False):
        return (obj['_linked_to'], )


class ToBigCommerceShipmentTask(ToBigCommerceTask):

    def __init__(self,
        from_class,
        client,
        to_type='Shipment',
        url_format='/orders/{}/shipments',
        conversion=None,
        where=None,
        with_reflection=False):

        super(ToBigCommerceShipmentTask, self).__init__(
            from_class=from_class,
            client=client,
            to_type=to_type,
            url_format=url_format,
            conversion=conversion,
            where=where,
            with_reflection=with_reflection)

    def _get_url(self, obj, is_update=False, data=None):
        url = self.url_format.format(self._get_order_id(obj, data=data))
        if is_update == True:
            url = '{}/{}'.format(url, self._get_shipment_id(obj, data=data))

        return url

    def _get_order_id(self, obj, data=None):
        raise Exception('not implemented yet')

    def _get_shipment_id(self, obj, data=None):
        raise Exception('not implemented yet')


class ToBigCommerceProductTask(ToBigCommerceTask):

    def __init__(self,
        from_class,
        client,
        to_type='Product',
        url_format='/products/{}',
        conversion=None,
        where=None,
        with_reflection=False):

        super(ToBigCommerceProductTask, self).__init__(
            from_class=from_class,
            client=client,
            to_type=to_type,
            url_format=url_format,
            conversion=conversion,
            where=where,
            with_reflection=with_reflection)

    def _get_url(self, obj, is_update=False, data=None):
        id = ''
        if is_update:
            id = obj.get('_linked_to').get('id')

        return self.url_format.format(id)
