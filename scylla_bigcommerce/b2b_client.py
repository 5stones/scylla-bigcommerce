# -*- coding: utf-8 -*-
"""Client to pull info from BundleB2B api"""
import scylla
import logging

class BundleB2bRestError(scylla.RestClient.RestError):
    pass


class BundleB2bRestClient(scylla.RestClient):
    _logger = logging.getLogger('scylla-bigcommerce')

    _successful_codes = [200, 201, 204]
    RestError = BundleB2bRestError

    def __init__(self, token, verify_ssl=True, debug=False):
        super(BundleB2bRestClient, self).__init__(
            "https://api.bundleb2b.net/api/v2/io/",
            verify_ssl=verify_ssl,
            debug=debug
        )

        self.name = "BundleB2b"
        self._set_header('authtoken', token)
        if debug:
            self._logger.setLevel(logging.DEBUG)

    def get_companies(self, after=None, limit=10, offset=0):
        params = {
            "sortBy": "updatedAt",
            "orderBy": "ASC",
            "limit": limit,
            "offset": offset,
        }
        if after:
            params['minLastModifiedTime'] = after
        return self.get('companies', params)

    def get_order_company(self, order_id):
        try:
            return self.get('orders/{}/companyIdBCId'.format(order_id))
        except:
            return None

    def get_company(self, company_id):
        return self.get('companies/{}/basic-info'.format(company_id))

    def get_company_for_order(self, order_id):
        order_company = self.get_order_company(order_id)
        if order_company is None:
            return (None, None)
        company = self.get_company(order_company['companyId'])
        return (order_company, company)

    def _process_response(self, response):
        result = super(BundleB2bRestClient, self)._process_response(response)
        if 'code' in result and result['code'] not in self._successful_codes:
            errorMessage = result.get('message', 'Error')
            raise self.RestError('{}: {}'.format(result['code'], errorMessage))
        return result['data']
