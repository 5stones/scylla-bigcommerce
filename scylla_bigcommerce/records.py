import logging
from datetime import datetime
import scylla
from .client import BigCommerceRestError

# recursion depth when initializing records
_process_depth = 0

# cache of records cleared after processing a response
_record_cache = {}


class BigCommerceRecord(scylla.ParsedRecord):
    _logger = logging.getLogger('scylla-bigcommerce')

    key_field = 'id'

    factory_base_classname = 'BigCommerce'
    classname = 'BigCommerceRecord'

    _sub_records = {}
    _linked_records = {}
    _datetime_fields = ['date_created', 'date_modified', 'date_shipped']

    def __init__(self, client, classname, obj, readonly_link=False):
        super(BigCommerceRecord, self).__init__(client, classname, obj, readonly_link)
        self.client = client
        self._readonly_link = readonly_link
        self.threepl_module = client.name
        if classname:
            self.classname = self.threepl_module + classname


    def _populate_subresources(self, record):

        if record is None or not isinstance(record, (list, dict)):
            return record

        # agnostic iterator over dict or list
        iterator = record if isinstance(record, dict) else xrange(len(record))

        for key in iterator:
            # check if the object is actually a nested resource
            if self._is_resource_ref(record[key]):
                try:
                    record[key] = self.client.get_resource(record[key])
                except BigCommerceRestError as exception:
                    # we can get a 404 when looking up resources
                    self._logger.warning('Could not populate %s: %s',
                                         record[key].get('resource'), exception)
                    record[key] = None

            if isinstance(record[key], dict):
                record[key] = self._populate_subresources(record[key])
            elif isinstance(record[key], list):
                items = []
                for item in record[key]:
                    items.append(self._populate_subresources(item))

                record[key] = items

        return record

    @staticmethod
    def _is_resource_ref(value):
        return isinstance(value, dict) and ('url' in value and 'resource' in value)

    def _process_fields(self):

        # pre-process datetimes
        for field in self._datetime_fields:
            if field in self.data:
                self.data[field] = self._convert_date_format(self.data[field])

        self.data = self._populate_subresources(self.data)

        super(BigCommerceRecord, self)._process_fields()

    def _convert_date_format(self, date):

        if date is None or date == '':
            return date

        d = datetime.strptime(date, '%a, %d %b %Y %H:%M:%S +0000')
        return '{}Z'.format(d.isoformat())


class BigCommerceCustomer(BigCommerceRecord):

    _sub_records = {
        'addresses': 'Address',
    }


class BigCommerceOrder(BigCommerceRecord):

    _sub_records = {
        'shipping_addresses': 'ShippingAddress',
        'products': 'LineItem',
        'shipments': 'Shipment',
    }

    def _process_fields(self):
        path = '/orders/{}/shipments'.format(self.data.get('id'))
        resource = { 'url': path, 'resource': path }
        self.data['shipments'] = resource
        super(BigCommerceOrder, self)._process_fields()


class BigCommerceShippingAddress(BigCommerceRecord):

    _sub_records = {
        'shipping_quotes': 'ShippingQuote',
    }


class BigCommerceProduct(BigCommerceRecord):
    _datetime_fields = ['date_created', 'date_modified', 'date_shipped', 'date_last_imported']

    _sub_records = {
        'skus': 'Sku',
        'options': 'Option',
        'images': 'Image',
        'primary_image': 'Image',
    }

class BigCommerceCountry(BigCommerceRecord):
    _datetime_fields = []

    _sub_records = {
        'states': 'State',
    }


class BigCommerceState(BigCommerceRecord):
    _datetime_fields = []


class BigCommerceCompany(BigCommerceRecord):
    """Company tracked in BundleB2B app."""
    _datetime_fields = []
    _timestamp_fields = ['updatedAt', 'createdAt']

    def _process_fields(self):
        self.data['id'] = self.data['companyId']
        # convert timestamps
        for field in self._timestamp_fields:
            if field in self.data:
                self.data[field] = datetime.fromtimestamp(int(self.data[field]))
        # define top-level indexable properties for extra fields
        if 'extraFields' in self.data:
            for field in self.data['extraFields']:
                field_name = '_ex_{}'.format(field['fieldName'])
                self.data[field_name] = field.get('fieldValue')
